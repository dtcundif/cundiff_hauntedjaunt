﻿using UnityEngine;

public class Teleport : MonoBehaviour
{
    public float FadeDuration = 1f;
    public float IdleDuration = 0.5f;
    public Vector3 Target;
    public GameObject Player;
    public CanvasGroup canvasGroup;

    private bool isPlayerTeleporting;
    private float timer;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (isPlayerTeleporting)
        {
            TeleportPlayer();
        }
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
            isPlayerTeleporting = true;
        }
    }

    void TeleportPlayer()
    {
        timer += Time.deltaTime;
        canvasGroup.alpha = timer / FadeDuration;

        if (timer > FadeDuration + IdleDuration)
        {
            isPlayerTeleporting = false;
            timer = 0;

            Player.transform.SetPositionAndRotation(Target, Player.transform.rotation);
            canvasGroup.alpha = 0;
        }
    }
}
