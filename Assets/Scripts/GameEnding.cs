﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameEnding : MonoBehaviour
{
    public float FadeDuration = 1f;
    public float DisplayImageDuration = 1f;
    public GameObject Player;
    public CanvasGroup exitBackgroundImageCanvasGroup;
    public CanvasGroup caughtBackgroundImageCanvasGroup;
    public AudioSource ExitAudio;
    public AudioSource CaughtAudio;

    private bool isPlayerAtExit;
    private bool isPlayerCaught;
    private float timer;
    private bool hasAudioPlayed;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player && Pickup.remainingLemons <= 0)
        {
            isPlayerAtExit = true;
        }
    }

    public void CaughtPlayer()
    {
        isPlayerCaught = true;
    }

    void Update()
    {
        if (isPlayerAtExit)
        {
            EndLevel(exitBackgroundImageCanvasGroup, false, ExitAudio);
        }
        else if (isPlayerCaught)
        {
            EndLevel(caughtBackgroundImageCanvasGroup, true, CaughtAudio);
        }
    }

    void EndLevel(CanvasGroup imageCanvasGroup, bool doRestart, AudioSource audioSource)
    {
        if (!hasAudioPlayed)
        {
            audioSource.Play();
            hasAudioPlayed = true;
        }

        timer += Time.deltaTime;
        imageCanvasGroup.alpha = timer / FadeDuration;

        if (timer > FadeDuration + DisplayImageDuration)
        {
            if (doRestart)
            {
                SceneManager.LoadScene(0);
                return;
            }

            Application.Quit();
        }
    }
}
