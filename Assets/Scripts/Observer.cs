﻿using UnityEngine;

public class Observer : MonoBehaviour
{
    public Transform Player;
    public GameEnding GameEnding;

    private bool isPlayerInRange;

    void OnTriggerEnter(Collider other)
    {
        if (other.transform == Player)
        {
            isPlayerInRange = true;
        }
    }

    void OnTriggerExit(Collider other)
    {
        if (other.transform == Player)
        {
            isPlayerInRange = false;
        }
    }

    void Update()
    {
        if (isPlayerInRange)
        {
            var direction = Player.position - transform.position + Vector3.up;
            var ray = new Ray(transform.position, direction);

            if (Physics.Raycast(ray, out var raycastHit))
            {
                if (raycastHit.collider.transform == Player)
                {
                    GameEnding.CaughtPlayer();
                }
            }
        }
    }
}
