﻿using UnityEngine;

public class Lightning : MonoBehaviour
{
    public Light Light;

    private float nextLightning;
    private float stopLightningTime;

    private void Start()
    {
        var now = Time.time;

        nextLightning = now + Random.Range(2, 5);
        stopLightningTime = now + 9999999;
    }

    void Update()
    {
        var now = Time.time;
        if (now >= nextLightning)
        {
            nextLightning = now + Random.Range(10, 15);
            stopLightningTime = now + Random.Range(0.1f, 0.2f);

            Light.intensity = Random.Range(5, 10);
        }

        if (now >= stopLightningTime)
        {
            stopLightningTime = now + 9999999;
            Light.intensity = 0;
        }
    }
}
