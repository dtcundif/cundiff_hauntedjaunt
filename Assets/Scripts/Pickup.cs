﻿using UnityEngine;
using UnityEngine.UI;

public class Pickup : MonoBehaviour
{
    public GameObject Player;
    public Text RemainingLemons;

    public static int remainingLemons;

    void Start()
    {
        remainingLemons = 5;
    }

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject == Player)
        {
            gameObject.SetActive(false);
            RemainingLemons.text = $"Remaining Lemons: {--remainingLemons}";
        }
    }
}
