﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{
    public float TurnSpeed = 20f;

    private Animator animator;
    private Rigidbody rigidBody;
    private AudioSource audioSource;
    private Vector3 movement;
    private Quaternion rotation = Quaternion.identity;

    // Start is called before the first frame update
    void Start()
    {
        animator = GetComponent<Animator>();
        rigidBody = GetComponent<Rigidbody>();
        audioSource = GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        var horizontal = Input.GetAxis("Horizontal");
        var vertical = Input.GetAxis("Vertical");

        movement.Set(horizontal, 0f, vertical);
        movement.Normalize();

        var hasHorizontalInput = !Mathf.Approximately(horizontal, 0f);
        var hasVerticalInput = !Mathf.Approximately(vertical, 0f);
        var isWalking = hasHorizontalInput || hasVerticalInput;

        if (isWalking)
        {
            if (!audioSource.isPlaying)
            {
                audioSource.Play();
            }
        }
        else
        {
            audioSource.Stop();
        }

        animator.SetBool("IsWalking", isWalking);

        var desiredForward = Vector3.RotateTowards(transform.forward, movement, TurnSpeed * Time.deltaTime, 0f);
        rotation = Quaternion.LookRotation(desiredForward);
    }

    void OnAnimatorMove()
    {
        rigidBody.MovePosition(rigidBody.position + movement * animator.deltaPosition.magnitude);
        rigidBody.MoveRotation(rotation);
    }
}
