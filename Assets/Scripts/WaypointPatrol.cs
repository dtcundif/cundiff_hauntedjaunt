﻿using UnityEngine;
using UnityEngine.AI;

public class WaypointPatrol : MonoBehaviour
{
    public NavMeshAgent NavMeshAgent;
    public Transform[] Waypoints;

    private int currentWaypointIndex;

    // Start is called before the first frame update
    void Start()
    {
        NavMeshAgent.SetDestination(Waypoints[0].position);
    }

    // Update is called once per frame
    void Update()
    {
        if (NavMeshAgent.remainingDistance < NavMeshAgent.stoppingDistance)
        {
            currentWaypointIndex = (currentWaypointIndex + 1) % Waypoints.Length;
            NavMeshAgent.SetDestination(Waypoints[currentWaypointIndex].position);
        }
    }
}
